var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;
  var path=require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);
app.get('/',function(request,response){
  //response.send('Hoola mundo');
  response.sendFile(path.join(__dirname,'index.html'));
});


app.get('/clientes/:idCliente',function(request,response){
  //response.send('Hoola mundo');
  response.json('Se ha recuperado el cliente: '+ request.params.idCliente);

});

app.post('/clientes/:idCliente',function(request,response){
  response.json('he recibido su peticion: '+ request.params.idCliente);

});

app.put('/clientes/:idCliente',function(request,response){
  response.send('se ha modificado el cliente: '+request.params.idCliente);
});

app.delete('/clientes/:idCliente',function(request,response){
  response.send('se ha eliminado el cliente: '+request.params.idCliente);
});
